import { ref } from '@vue/reactivity';

const getPosts = () => {
    const posts = ref([])
    const err = ref(null)

    const loadData = async () => {
        try {
            let data = await fetch('http://localhost:3000/posts')
            console.log(data);
            if(!data.ok){
                throw Error("No data available")
            }else {
                posts.value = await data.json()
            }
        } catch (error) {
            err.value = error.message
        }
    }

    return { posts, err, loadData }
}

export default getPosts
import { createRouter, createWebHistory } from 'vue-router'
import Part1 from '../views/Part1.vue'
import Part2 from '../views/Part2.vue'

const routes = [
  {
    path: '/part1',
    name: 'Part1',
    component: Part1
  },
  {
    path: '/part2',
    name: 'Part2',
    component: Part2
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
